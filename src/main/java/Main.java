
import fr.realcorporation.Services.PersonService;
import fr.realcorporation.tpjava.POJO.Address;
import fr.realcorporation.tpjava.POJO.Person;
import fr.realcorporation.tpjava.POJO.Place;
import fr.realcorporation.tpjava.POJO.Student;
import fr.realcorporation.tpjava.POJO.Teacher;
import io.undertow.Handlers;
import io.undertow.Undertow;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
@author rthigoulet
 */
public class Main {

    private static Logger log = Logger.getLogger("Main");
    private static Scanner scanner = new Scanner(System.in);

    public static void main(final String[] args) {

        Undertow server = Undertow.builder().addHttpListener(8888, "localhost")
                .setHandler(Handlers.path()
                        .addPrefixPath("/person", Handlers.routing()
                                
                                .get("/read", PersonService::getRead))
                        
                ).build();
        server.start();
    }
}

/*String moi = args[0];
        int n;
        do {
            n = demanderNombre();
            int f = factor(n);
            log.log(Level.INFO, "La valeur est : " + factor(n));// Affiche le résultat de la factorisation
        } while (n != 0); //Si n = 0 on sort du programme
        log.log(Level.INFO, moi); //Affiche le message args
        
        /* Début Exercice 3
        Address address = new Address(77, "rue", "Grande Saint-Christophe", 36000, "Châteeauroux", "France");
        log.log(Level.INFO, address.toString());
        /* Fin Exercice 3*/
 /* Début Exercice 4
        Place place = new Place("Maison Romain", address, Place.TypePlace.Studio, "Trop trop rigolo !");
        log.log(Level.INFO,place.toString());
        /* Fin Exercice 4*/
 /* Début Exercice 5
        Person person = new Person("Thigoulet", "Romain","France", "Nothing");
        log.log(Level.INFO, person.toString());
        /* Fin Exercice 5*/
 /* Début Exercice 6
        Teacher teacher = new Teacher("Dagniau", "Marie", "France", "Java", "Java");
        log.log(Level.INFO, teacher.toString());
        
        Student student = new Student("Fontaine", "Clément", "France", "Nothing", "SIO");
        log.log(Level.INFO, student.toString());
        /* Fin Exercice 6
}
    
    
    /*
    Exercice 2 sur le scanner pour demander a l'utilisateur de saisir un nombre
     
    static int demanderNombre() {
        int n;
        System.out.println("Donnez un nombre entier si '0' cela quitte le programme");
        n = scanner.nextInt();
        return n;
    }
    /*
    Factorisation itérative
    
    static int factor(int n) {
        int fact = 1;
        for (int i = 2; i <= n; i++) {
            fact *= i;
        }
        return fact;
    
    }
}














/*
    Exercice 1 sur les factorielle : 

    public static int factor(int n) {

        int fact = 1;
        for (int i = 2; i <= n; i++) { // Calcul de la factorielle par la méthode itérative
            fact *= i;
        }
        return fact;
    }
 */
