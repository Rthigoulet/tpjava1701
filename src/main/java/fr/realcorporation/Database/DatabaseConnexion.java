/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.Database;

import fr.realcorporation.Utils.MessageUtils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

/**
 *
 * @author rthigoulet
 */
public class DatabaseConnexion {

    public static Connection connexion() {
        try {
            Properties prop = MessageUtils.load("properties.properties");

            String db = prop.getProperty("db");
            String host = prop.getProperty("host");
            String port = prop.getProperty("port");
            String dbname = prop.getProperty("dbname");
            String user = prop.getProperty("dbuser");
            String pw = prop.getProperty("dbpassword");

            Connection con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + dbname + "?user=" + user + "&password=" + pw + "&serverTimezone=Europe/Paris");

            return con;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static ResultSet query(String query) {
        try {
            Connection conn = connexion();
            Statement statement = conn.createStatement();
            ResultSet resultat = null;
            if(statement.execute(query)){
                resultat = statement.getResultSet();
            }
            
            return resultat;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
