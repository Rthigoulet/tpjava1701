/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.Services;

import fr.realcorporation.Utils.FormatUtils;
import fr.realcorporation.tpjava.DAO.PersonDAO;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import java.sql.ResultSet;

/**
 *
 * @author rthigoulet
 */
public class PersonService {
    
    public static void getRead(HttpServerExchange hse){
        ResultSet resultset = PersonDAO.getPersonList();
        hse.getResponseHeaders().put(Headers.CONTENT_TYPE,"text/plain");
        hse.getResponseHeaders().send(FormatUtils.parseToJson(resultset));
        
                
}
}
