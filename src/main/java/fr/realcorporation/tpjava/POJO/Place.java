/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tpjava.POJO;

/**
 *
 * @author rthigoulet
 */
public class Place {
    
    String nom;
    Address address;
    TypePlace type;
    String note;
    
    
    
    public enum TypePlace{
        Appartement, Immeuble, Studio, Maison, Bar, Restaurant;
    }

    public Place(String nom, Address address, TypePlace type, String note) {
        this.nom = nom;
        this.address = address;
        this.type = type;
        this.note = note;
    }

    @Override
    public String toString() {
        return "Place{" + "nom=" + nom + ", address=" + address + ", type=" + type + ", note=" + note + '}';
    }
    
    
    
}
