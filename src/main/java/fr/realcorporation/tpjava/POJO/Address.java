/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tpjava.POJO;

/**
 *
 * @author rthigoulet
 */
public class Address {
    
    int number;
    String adr1;
    String adr2;
    int cp;
    String city;
    String country;
    float longitude;
    float latitute;

    public Address(int number, String adr1, String adr2, int cp, String city, String country, float longitude, float latitute) {
        this.number = number;
        this.adr1 = adr1;
        this.adr2 = adr2;
        this.cp = cp;
        this.city = city;
        this.country = country;
        this.longitude = longitude;
        this.latitute = latitute;
    }

    public Address(int number, String adr1, String adr2, int cp, String city, String country) {
        this.number = number;
        this.adr1 = adr1;
        this.adr2 = adr2;
        this.cp = cp;
        this.city = city;
        this.country = country;
    }

    @Override
    public String toString() {
        return "Address{" + "number=" + number + ", adr1=" + adr1 + ", adr2=" + adr2 + ", cp=" + cp + ", city=" + city + ", country=" + country + ", longitude=" + longitude + ", latitute=" + latitute + '}';
    }
    
    
    
    
    
}
