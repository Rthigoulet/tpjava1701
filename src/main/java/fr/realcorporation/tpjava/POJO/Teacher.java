/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tpjava.POJO;

/**
 *
 * @author rthigoulet
 */
public class Teacher extends Person {
    
    String subject;
    
    public Teacher(String name, String firstname, String Country, String Activity, String subject) {
        super(name, firstname, Country, Activity);
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Teacher{" + "name=" + name + ", firstname=" + firstname + ", Country=" + Country + ", Activity=" + Activity + "subject=" + subject + '}';
    }
}
