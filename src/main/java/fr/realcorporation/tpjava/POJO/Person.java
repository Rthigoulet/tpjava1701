/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tpjava.POJO;

/**
 *
 * @author rthigoulet
 */
public class Person {
    
    String name;
    String firstname;
    String Country;
    String Activity;

    public Person(String name, String firstname, String Country, String Activity) {
        this.name = name;
        this.firstname = firstname;
        this.Country = Country;
        this.Activity = Activity;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", firstname=" + firstname + ", Country=" + Country + ", Activity=" + Activity + '}';
    }
    
    
    
    
}
