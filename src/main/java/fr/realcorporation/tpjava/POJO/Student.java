/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.tpjava.POJO;

/**
 *
 * @author rthigoulet
 */
public class Student extends Person{
    
    String classe;
    
    public Student(String name, String firstname, String Country, String Activity, String classe) {
        super(name, firstname, Country, Activity);
        this.classe = classe;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", firstname=" + firstname + ", Country=" + Country + ", Activity=" + Activity + "classe=" + classe + '}';
    }
}
