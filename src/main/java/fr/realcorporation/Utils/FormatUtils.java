/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.realcorporation.Utils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/**
 *
 * @author rthigoulet
 */
public class FormatUtils {
    
    static int factor(int n) {
        int fact = 1;
        for (int i = 2; i <= n; i++) {
            fact *= i;
        }
        return fact;
    
    }
    
    
    public static JSONObject parseToJsonRs(ResultSet rs){
        Map<String, String> m = new HashMap<>();
        ResultSetMetaData meta = rs.getMetaData();
        while (rs.next()){
            for (int i = 1; i < meta.getColumnCount() + 1; )
                m.put(meta.getColumnName(i), rs.getObject(i))
        }
    }
    JSONObject jo = new JSONObject(m);
    return jo;
}
